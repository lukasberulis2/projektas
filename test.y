%{
#include <stdio.h>
#include <string.h>

#define YYSTYPE char *

int yydebug=0;

void yyerror(const char *str)
{
	fprintf(stderr,"error: %s\n",str);
}

int yywrap()
{
	return 1;
}

main()
{
	yyparse();
}

%}

%token WORD WHILETOK QUOTE SEMICOLON NUMBER IFTOK EQCOMPARATOR BOOLEANTOK SYSTEM

%%

commands:
	|	 
	commands command SEMICOLON
	;

command:
	NUMBER {
		printf("Your entered a number - %d", $1);
	} | WHILETOK NUMBER string {
		int i = 0;
		while (i < $2) {
			printf("Looping - %s\n", $3);
			i++;
		}
	} | IFTOK boolean string {
		if ($2) {
			printf("If - %s\n", $3);
		}
	} | SYSTEM string {
		system($2);
	}
	;
	
string:
	QUOTE WORD QUOTE {
		$$=$2;
	}
	;
	
boolean:
	NUMBER EQCOMPARATOR NUMBER {
		$$ = $1 == $3;
	} | string EQCOMPARATOR string {
		$$ = strcmp($1, $3);
	} | BOOLEANTOK {
		$$ = $1;
	}
	;