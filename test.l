%{
#include <stdio.h>
#include "y.tab.h"
%}

%%

system 					return SYSTEM;
true|false				yylval=!strcmp(yytext, "true"); return BOOLEANTOK;
==						return EQCOMPARATOR;
mif						return IFTOK;
while					return WHILETOK;
[0-9]+					yylval=atoi(yytext); return NUMBER;
[a-zA-Z][a-zA-Z0-9]*    yylval=strdup(yytext); return WORD;
\"                      return QUOTE;
;                       return SEMICOLON;
%%
